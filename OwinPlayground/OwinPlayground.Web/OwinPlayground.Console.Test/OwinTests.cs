﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Owin;

namespace OwinPlayground.Console.Test
{
    [TestClass]
    public class OwinTests
    {
        [TestMethod]
        public async Task ShouldReturn200OnRequestToRoot()
        {
            var statusCode = await CallServer(async x =>
            {
                var response = await x.GetAsync("/");
                return response.StatusCode;
            });
            Assert.AreEqual(HttpStatusCode.OK, statusCode);
        }

        [TestMethod]
        public async Task ShouldReturnHelloWorld()
        {
            var body = await CallServer(async x =>
            {
                var response = await x.GetAsync("/");
                return await response.Content.ReadAsStringAsync();
            });
            Assert.AreEqual("Hello World!", body);
        }

        private async Task<T> CallServer<T>(Func<HttpClient, Task<T>> callback)
        {
            using (var server = TestServer.Create<Startup>())
            {
                return await callback(server.HttpClient);
            }
        } 
    }
}
