﻿using System.Diagnostics;
using Microsoft.Owin;
using Owin;
using OwinPlayground.Web.Middleware;

[assembly: OwinStartupAttribute(typeof(OwinPlayground.Web.Startup))]
namespace OwinPlayground.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            //  Own Simple Middleware using Katana (2)
            app.UseDebugMiddleware(new DebugMiddlewareOptions()
            {
                OnIncomingRequest = (ctx) =>
                {
                    var watch = new Stopwatch();
                    watch.Start();
                    ctx.Environment["DebugStopwatch"] = watch;
                },
                OnOutgoingRequest = (ctx) =>
                {
                    var watch = (Stopwatch)ctx.Environment["DebugStopwatch"];
                    watch.Stop();
                    Debug.WriteLine("Request took: " + watch.ElapsedMilliseconds + " ms.");
                }
            });

            //  Own Simple Middleware using Katana (2)
            app.Use(async (ctx, next) => { await ctx.Response.WriteAsync("Hello World!"); });
        }
    }
}
