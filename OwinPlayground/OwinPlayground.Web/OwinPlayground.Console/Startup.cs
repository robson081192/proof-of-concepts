﻿using Owin;

namespace OwinPlayground.Console
{
    public class Startup
    {
        public static void Configuration(IAppBuilder app)
        {
            app.UseStaticFiles();
            app.Use(async (ctx, next) => { await ctx.Response.WriteAsync("Hello World!"); });
        }   
    }
}
