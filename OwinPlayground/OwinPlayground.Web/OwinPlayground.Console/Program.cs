﻿using Microsoft.Owin.Hosting;

namespace OwinPlayground.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WebApp.Start<Startup>("http://localhost:12345"))
            {
                System.Console.WriteLine("Listening to port 12345");
                System.Console.WriteLine("Press Enter to end...");
                System.Console.ReadLine();
            }
        }
    }
}
