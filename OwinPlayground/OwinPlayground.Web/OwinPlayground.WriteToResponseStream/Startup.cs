﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Owin;

namespace OwinPlayground.WriteToResponseStream
{
    public class Startup
    {
        public static void Configuration(IAppBuilder app)
        {
            //  first middleware (write some content)
            app.Use(async (ctx, next) =>
            {
                await ctx.Response.WriteAsync("First message");
                await next();
            });

            //  second middleware (depending on content change it)
            app.Use(async (ctx, next) =>
            {
                var originalStream = ctx.Response.Body;
                try
                {
                    using (var stream = new MemoryStream())
                    {
                        ctx.Response.Body = stream;
                        await next();
                        stream.Position = 0;
                        await new StreamReader(stream).ReadToEndAsync();
                        var buffer = Encoding.ASCII.GetBytes("\nSecond message");
                        await ctx.Response.Body.WriteAsync(buffer, 0, buffer.Length);
                        stream.Position = 0;
                        await stream.CopyToAsync(originalStream);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    ctx.Response.Body = originalStream;
                }
            });

            //  third middleware
            app.Use(async (ctx, next) =>
            {
                await ctx.Response.WriteAsync("\nThird message");
                await next();
            });
        }
    }
}
