﻿using ApplicationTesting.Factories;

namespace UnitTests.UserFactoryTests
{
    public class UserFactoryFixture
    {
        private static readonly object Lock = new object();
        public UserFactoryFixture()
        {
            Factory = new UserFactory();
            lock (Lock)
            {
                Counter++;
            }
        }

        public UserFactory Factory { get; }
        public static int Counter { get; private set; }
    }
}
