﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ApplicationTesting.Factories;
using ApplicationTesting.Models.Users;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests.UserFactoryTests
{
    public class UserFactoryShould: IClassFixture<UserFactoryFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly UserFactoryFixture _userFactoryFixture;
        private readonly UserFactory _sut;

        public UserFactoryShould(ITestOutputHelper output, UserFactoryFixture userFactoryFixture)
        {
            _output = output;
            //  1. Arrange
            _userFactoryFixture = userFactoryFixture;
            _sut = _userFactoryFixture.Factory;
        }

        [Theory]
        [InlineData(true, nameof(Administrator), 2000)]
        [InlineData(false, nameof(CommonUser), 5000)]
        public void CreateProperUser(bool isAdministrator, string type, int delay)
        //public void CreateProperUser()
        {
            UserType userType;
            Enum.TryParse(type, out userType);
            Thread.Sleep(delay); 
            //  2. Act
            var user = _sut.Create("Jan", "Kowalski", 50, isAdministrator);

            //  3. Assert
            if (isAdministrator)
            {
                Assert.IsType<Administrator>(user);
            }
            else
            {
                Assert.IsType<CommonUser>(user);
            }
            Assert.Equal(userType, user.UserType);
            _output.WriteLine($"Counter: {UserFactoryFixture.Counter}");
        }

        [Theory]
        [InlineData(true, nameof(Administrator), 2000)]
        [InlineData(false, nameof(CommonUser), 5000)]
        public void Duplicate(bool isAdministrator, string type, int delay)
            //public void CreateProperUser()
        {
            UserType userType;
            Enum.TryParse(type, out userType);
            Thread.Sleep(delay);
            //  2. Act
            var user = _sut.Create("Jan", "Kowalski", 50, isAdministrator);

            //  3. Assert
            if (isAdministrator)
            {
                Assert.IsType<Administrator>(user);
            }
            else
            {
                Assert.IsType<CommonUser>(user);
            }
            Assert.Equal(userType, user.UserType);
            _output.WriteLine($"Counter: {UserFactoryFixture.Counter}");
        }
    }
}

