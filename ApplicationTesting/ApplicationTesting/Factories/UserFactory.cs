﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationTesting.Models.Users;

namespace ApplicationTesting.Factories
{
    public class UserFactory
    {
        public User Create(string firstName, string lastName, int age, bool isAdministrator = false)
        {
            if (isAdministrator)
            {
                return new Administrator(firstName, lastName, age);
            }
            return new CommonUser(firstName, lastName, age);
        }
    }
}
