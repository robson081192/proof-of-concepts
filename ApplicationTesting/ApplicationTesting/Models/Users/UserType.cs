﻿namespace ApplicationTesting.Models.Users
{
    public enum UserType
    {
        Administrator,
        CommonUser
    }
}
