﻿namespace ApplicationTesting.Models.Users
{
    public abstract class User
    {
        protected User(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

        public string FirstName { get; }
        public string LastName { get; }
        public int Age { get; }

        public abstract string Whoami();
        public abstract UserType UserType { get; }
    }
}
