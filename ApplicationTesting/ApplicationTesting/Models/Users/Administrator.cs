﻿namespace ApplicationTesting.Models.Users
{
    public class Administrator: User
    {
        public Administrator(string firstName, string lastName, int age): base(firstName, lastName, age)
        {
            
        }
        public override string Whoami()
        {
            return "Hello, I'm an Administrator.";
        }

        public override UserType UserType => UserType.Administrator;
    }
}
