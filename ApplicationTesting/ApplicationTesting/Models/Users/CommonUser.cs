﻿namespace ApplicationTesting.Models.Users
{
    public class CommonUser: User
    {
        public CommonUser(string firstName, string lastName, int age) : base(firstName, lastName, age)
        {

        }
        public override string Whoami()
        {
            return "Hello, I'm a CommonUser.";
        }

        public override UserType UserType => UserType.CommonUser;
    }
}
